use std::{io, process::Command};

fn main() -> io::Result<()> {
    let gresource_out = "com.gitlab.miridyan.Mt.Nx.gresource";
    let gresource_xml = "com.gitlab.miridyan.Mt.Nx.gresource.xml";
    let cargo_target_dir = option_env!("CARGO_TARGET_DIR")
        .unwrap_or_else(|| concat!(env!("CARGO_MANIFEST_DIR"), "/target"));
    // let cargo_target_dir = match option_env!("CARGO_TARGET_DIR") {
    //     Some(target_dir) => target_dir,
    //     None => match 
    // };
    // let cargo_target_dir = option_env!("CARGO_TARGET_DIR");
    // let cargo_target_dir =
    //     option_env!("CARGO_TARGET_DIR")
    //         .unwrap_or(concat!(option_env!"/target"));

    println!("cargo:rustc-env=GRESOURCE_OUT_DIR={cargo_target_dir}");
    println!("cargo:rerun-if-changed=./res/{gresource_xml}");
    println!("cargo:rerun-if-changed={cargo_target_dir}/{gresource_out}");

    Command::new("glib-compile-resources")
        .current_dir("res")
        .arg(format!("--target={cargo_target_dir}/{gresource_out}"))
        .arg(gresource_xml)
        .output()?;

    Ok(())
}
