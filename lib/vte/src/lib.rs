#![cfg_attr(feature = "dox", feature(doc_cfg))]

// pub use ffi;
pub use gtk;

// no runtime to initialize
macro_rules! assert_initialized_main_thread {
    () => {};
}

// No-op
macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
mod auto;
pub use auto::*;

pub mod prelude;
pub use prelude::*;

mod terminal;

