use gtk::glib;
use gtk::glib::object::IsA;
use gtk::glib::translate::*;

use gdk::ffi as gdk_sys;
use vte_sys as ffi;

use crate::Terminal;

pub trait TerminalExtManual: 'static {
    #[doc(alias = "vte_terminal_set_colors")]
    fn set_colors(
        &self,
        foreground: Option<&gdk::RGBA>,
        background: Option<&gdk::RGBA>,
        palette: &[&gdk::RGBA],
    );

    #[doc(alias = "vte_terminal_watch_child")]
    fn watch_child(&self, child_pid: glib::Pid);
}

impl<O: IsA<Terminal>> TerminalExtManual for O {
    #[doc(alias = "vte_terminal_set_colors")]
    fn set_colors(
        &self,
        foreground: Option<&gdk::RGBA>,
        background: Option<&gdk::RGBA>,
        palette: &[&gdk::RGBA],
    ) {
        let palette_size = palette.len() as usize;

        let palette_vector = palette
            .iter()
            .map(|item| unsafe { *item.to_glib_none().0 })
            .collect::<Vec<gdk_sys::GdkRGBA>>();

        unsafe {
            ffi::vte_terminal_set_colors(
                self.as_ref().to_glib_none().0,
                foreground.to_glib_none().0,
                background.to_glib_none().0,
                palette_vector.as_ptr(),
                palette_size,
            );
        }
    }

    fn watch_child(&self, child_pid: glib::Pid) {
        unsafe {
            ffi::vte_terminal_watch_child(self.as_ref().to_glib_none().0, child_pid.into_glib());
        }
    }
}

