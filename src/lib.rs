#![feature(once_cell)]

mod app;
pub mod config;
mod error;
pub mod traits;
pub mod widget;
pub mod window;

#[doc(inline)]
pub use {app::*, error::*};

pub mod event {
    use std::path::PathBuf;

    #[derive(Debug)]
    pub enum AppEvent {
        ConfigFileWatcherStarted,
        ConfigFileChanged(Vec<PathBuf>),
    }
}
