use std::{cell, fmt, str};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    // GLibError(glib::Error),
    // GLibBoolError(glib::BoolError),
    UTF8Error(str::Utf8Error),
    IOError(std::io::Error),
    RonError(ron::Error),
    RegexError(regex::Error),
    BorrowError(cell::BorrowError),
    BorrowMutError(cell::BorrowMutError),
    NotifyError(notify::Error),
    XdgError(xdg::BaseDirectoriesError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            // Self::GLibError(e) => e.fmt(f),
            Self::UTF8Error(e) => e.fmt(f),
            // Self::GLibBoolError(e) => e.fmt(f),
            Self::IOError(e) => e.fmt(f),
            Self::RonError(e) => e.fmt(f),
            Self::RegexError(e) => e.fmt(f),
            Self::BorrowError(e) => e.fmt(f),
            Self::BorrowMutError(e) => e.fmt(f),
            Self::NotifyError(e) => e.fmt(f),
            Self::XdgError(e) => e.fmt(f),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::UTF8Error(e) => Some(e),
            Self::IOError(e) => Some(e),
            Self::RonError(e) => Some(e),
            Self::RegexError(e) => Some(e),
            Self::BorrowError(e) => Some(e),
            Self::BorrowMutError(e) => Some(e),
            Self::NotifyError(e) => Some(e),
            Self::XdgError(e) => Some(e),
        }
    }
}

// impl From<glib::Error> for Error {
//     fn from(e: glib::Error) -> Self {
//         Self::GLibError(e)
//     }
// }

// impl From<glib::BoolError> for Error {
//     fn from(e: glib::BoolError) -> Self {
//         Self::GLibBoolError(e)
//     }
// }

impl From<str::Utf8Error> for Error {
    fn from(e: str::Utf8Error) -> Self {
        Self::UTF8Error(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::IOError(e)
    }
}

impl From<ron::Error> for Error {
    fn from(e: ron::Error) -> Self {
        Self::RonError(e)
    }
}

impl From<regex::Error> for Error {
    fn from(err: regex::Error) -> Self {
        Error::RegexError(err)
    }
}

impl From<cell::BorrowError> for Error {
    fn from(err: cell::BorrowError) -> Self {
        Self::BorrowError(err)
    }
}

impl From<cell::BorrowMutError> for Error {
    fn from(err: cell::BorrowMutError) -> Self {
        Self::BorrowMutError(err)
    }
}

impl From<notify::Error> for Error {
    fn from(err: notify::Error) -> Self {
        Self::NotifyError(err)
    }
}

impl From<xdg::BaseDirectoriesError> for Error {
    fn from(err: xdg::BaseDirectoriesError) -> Self {
        Self::XdgError(err)
    }
}
