use {
    crate::{
        config::{file, ColorScheme, Config, Profile},
        event::AppEvent,
        window::ApplicationWindow,
    },
    adw::subclass::prelude::*,
    gtk::{
        gio::{resources_register, Resource},
        glib::{self, Bytes, MainContext, Receiver, Sender},
        prelude::*,
        subclass::prelude::GtkApplicationImpl,
        CssProvider, Window,
    },
    std::cell::RefCell,
};

const GRESOURCES: &'static [u8] = include_bytes!(concat!(
    env!("GRESOURCE_OUT_DIR"),
    "/com.gitlab.miridyan.Mt.Nx.gresource"
));

#[derive(Debug)]
pub struct Application {
    event_loop: RefCell<Option<Receiver<AppEvent>>>,
    pub(super) watcher: file::Watcher,
    pub(super) event_sender: Sender<AppEvent>,
    pub(super) config: RefCell<Config>,
    pub(super) provider: RefCell<Option<CssProvider>>,
}

impl Default for Application {
    fn default() -> Self {
        log::trace!("+Application::default()");
        let (sender, receiver) =
            MainContext::channel::<AppEvent>(glib::PRIORITY_DEFAULT);
        let watcher = file::Watcher::new(sender.clone())
            .expect("Failed to create new_watcher");

        let container = watcher
            .read()
            .map(|file| Config::from(file))
            .unwrap_or_default();

        log::trace!("the container {container:#?}");

        let app = Self {
            event_loop: RefCell::new(Some(receiver)),
            config: RefCell::new(container),
            event_sender: sender,
            provider: RefCell::new(None),
            watcher,
        };

        log::trace!("-Application::default() -> {app:?}");
        app
    }
}

#[glib::object_subclass]
impl ObjectSubclass for Application {
    const NAME: &'static str = "MtApplication";

    type Type = super::Application;
    type ParentType = adw::Application;
}

impl ObjectImpl for Application {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        obj.setup_accelerators();
    }
}

impl ApplicationImpl for Application {
    /// Startup parent class `AdwApplication`, create and load gresources
    /// from embedded gresource data, and start the main listener for the
    /// config file watcher.
    fn startup(&self, application: &Self::Type) {
        log::trace!("+Application::startup(application: {application:?})");
        self.parent_startup(application);

        let resource = Resource::from_data(&Bytes::from_static(GRESOURCES))
            .expect("Failed to create resources");
        resources_register(&resource);

        let _ = self
            .config
            .try_borrow()
            .map(|config| {
                application.load_styles(&config);
            })
            .map_err(|err| log::error!("{err:?}"));

        let main_event_loop = self
            .event_loop
            .try_borrow_mut()
            .expect("Failed to borrow event_loop as mutable")
            .take()
            .expect("Failed to unwrap event_loop, already attached");

        main_event_loop.attach(
            None,
            glib::clone!(@strong application => @default-return Continue(false),
                move |event| application.handle_event(event)
            ),
        );

        if let Err(err) = self.watcher.start() {
            log::error!("Failed to start new watcher {err}");
        }

        log::trace!("-Application::startup()");
    }

    /// Activate parent class `AdwApplication`, create new `ApplicationWindow`,
    /// and grab focus.
    fn activate(&self, application: &Self::Type) {
        log::trace!("+Application::activate(application: {application:?})");
        self.parent_activate(application);

        if cfg!(debug_assertions) {
            Window::set_interactive_debugging(true);
            log::trace!("debug_assertions=true, enabling interactive debugger");
        }

        let window = ApplicationWindow::new(application);
        window.present();
        window.create_tab(None);
        log::trace!("-Application::activate()")
    }
}

impl GtkApplicationImpl for Application {}
impl AdwApplicationImpl for Application {}

impl Application {
    pub fn profile(&self, profile: &str) -> Option<Profile> {
        let config = self.config.borrow();
        let profile = config.profile(profile)?;

        Some(profile.clone())
    }

    pub fn colorscheme(&self, colorscheme: &str) -> Option<ColorScheme> {
        let config = self.config.borrow();
        let colorscheme = config.colorscheme(colorscheme)?;

        Some(colorscheme.clone())
    }
}
