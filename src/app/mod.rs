mod imp;

use {
    crate::{
        config::{file, ColorScheme, Config, Profile},
        event::AppEvent,
        window::ApplicationWindow,
    },
    gtk::{
        gdk::Display,
        gio::{self, ResourceLookupFlags},
        glib::{self, Sender},
        prelude::*,
        subclass::prelude::*,
        CssProvider, StyleContext,
    },
    std::collections::HashMap,
};

#[cfg(not(debug_assertions))]
pub const APPID: &'static str = "com.gitlab.miridyan.Mt.Nx";
#[cfg(debug_assertions)]
pub const APPID: &'static str = "com.gitlab.miridyan.Mt.Nx-Devel";

type BorrowResult<'a, T> =
    Result<std::cell::Ref<'a, T>, std::cell::BorrowError>;

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Application {
    /// Create a new `Application` instance. If compiled with `--release`, the App Id will be
    /// `com.gitlab.miridyan.Mt.Nx`, otherwise it will be `com.gitlab.miridyan.Mt.Nx.Devel`.
    /// This is Id was chosed to prevent the `release` and `debug` versions from interfering
    /// with eachother, as well as to prevent it from interfering with the `gtk3` version.
    pub fn new() -> Self {
        log::trace!("+Application::new()");
        let app = glib::Object::new::<Self>(&[
            ("application-id", &APPID),
            ("flags", &gio::ApplicationFlags::empty()),
            ("resource-base-path", &"/com/gitlab/miridyan/Mt/Nx"),
        ]);

        log::trace!("-Application::new() -> {app:?}");
        app
    }

    // make this better
    fn handle_event(&self, event: AppEvent) -> Continue {
        match event {
            // When the config file is written to by vim, we receive two write events,
            // and inbetween those two events there is a file deletion. There's a data
            // race sometimes where this loop is trying to reload the file but it gets
            // deleted, resulting in a failure. We don't crash on a failure, so when the
            // second event arrives we reload successfully, but this is still pretty
            // ugly and that should be fixed.
            AppEvent::ConfigFileChanged(files) => {
                for file in files.iter() {
                    if file == self.watcher().path() {
                        log::info!("The right file was reloaded {file:?}");
                        let _ = self.reload_config();
                    }
                }
            },
            e @ _ => {
                log::info!("Unimplemented Event received: {e:?}");
            }
        }

        Continue(true)
    }

    // perhaps this could be asynchronous
    fn reload_config(&self) -> crate::Result<()> {
        let file = self.imp().watcher.read()?;
        let mut config = self.imp().config.try_borrow_mut()?;

        *config = Config::from(file);

        let old_styles = self.load_styles(&config);
        for window in self.windows() {
            if let Some(w) = window.downcast_ref::<ApplicationWindow>() {
                w.reload_config(&config);
            }
        }

        if let Some(old_provider) = old_styles {
            StyleContext::remove_provider_for_display(
                &Display::default().expect("Failed to get display"),
                &old_provider,
            );
        }

        Ok(())
    }

    /// TODO(option-to-result-overhaul)
    ///
    /// Load a new stylesheet and register it as a provider for the entire
    /// application. Takes a reference to the config, does not borrow config
    /// from `self`. This is the only function that should borrow the provider
    /// cell, whether mutably or immutably. This function will create a new
    /// CssProvider and register it against the gdk display for the app, and
    /// will return the previously stored provider, without unregistering it
    /// from the display. Unregistering the returned provider is left up to the
    /// caller. If the provider is dropped after it has been returned from This
    /// function, there is no way to unregister it.
    fn load_styles(&self, config: &Config) -> Option<CssProvider> {
        let mut provider_cell = self.imp().provider.try_borrow_mut().ok()?;

        let provider = CssProvider::new();
        let stylesheets = Self::load_stylesheets(config)?;
        provider.load_from_data(stylesheets.as_bytes());
        StyleContext::add_provider_for_display(
            &Display::default()?,
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        let old_provider = provider_cell.replace(provider);
        old_provider
    }

    /// TODO(option-to-result-overhaul)
    ///
    /// Load the base stylesheet and generate the colorscheme specific
    /// styles. If no stylesheet could be generated or we failed to load a
    /// resource somewhere along the way, this function will return `None`.
    fn load_stylesheets(config: &Config) -> Option<String> {
        let resource = gio::resources_lookup_data(
            "/com/gitlab/miridyan/Mt/Nx/base.css",
            ResourceLookupFlags::NONE,
        )
        .ok()?;

        let mut stylesheet_base =
            std::str::from_utf8(&resource).ok()?.to_string();

        let style_classes = config.styles();
        let stylesheets = config.stylesheet(&style_classes)?;

        stylesheet_base.push_str("\n");
        stylesheet_base.push_str(&stylesheets);

        Some(stylesheet_base)
    }

    /// Given a closure `FnMut(&gtk::Window)`, execute that closure on all windows associated with
    /// the application, whether said window is a `window::ApplicationWindow` or some other type.
    /// Downcasting to a specific window type must be handled within the closure.
    pub fn for_each_window<F>(&self, f: F)
    where
        F: FnMut(&gtk::Window),
    {
        self.windows().iter().for_each(f);
    }

    pub fn profile(&self, profile: &str) -> Option<Profile> {
        self.imp().profile(profile)
    }

    pub fn colorscheme(&self, colorscheme: &str) -> Option<ColorScheme> {
        self.imp().colorscheme(colorscheme)
    }

    pub fn config(&self) -> BorrowResult<Config> {
        self.imp().config.try_borrow()
    }

    pub fn sender(&self) -> Sender<AppEvent> {
        self.imp().event_sender.clone()
    }

    pub fn styles(&self) -> Option<HashMap<String, String>> {
        self.config().ok().map(|c| c.styles())
    }

    fn watcher(&self) -> &file::Watcher {
        &self.imp().watcher
    }

    fn setup_accelerators(&self) {
        self.set_accels_for_action("win.new-tab", &["<primary><shift>t"]);
        self.set_accels_for_action(
            "win.close-current-tab",
            &["<primary><shift>w"],
        );

        self.set_accels_for_action("win.select-tab(0)", &["<alt>1"]);
        self.set_accels_for_action("win.select-tab(1)", &["<alt>2"]);
        self.set_accels_for_action("win.select-tab(2)", &["<alt>3"]);
        self.set_accels_for_action("win.select-tab(3)", &["<alt>4"]);
        self.set_accels_for_action("win.select-tab(4)", &["<alt>5"]);
        self.set_accels_for_action("win.select-tab(5)", &["<alt>6"]);
        self.set_accels_for_action("win.select-tab(6)", &["<alt>7"]);
        self.set_accels_for_action("win.select-tab(7)", &["<alt>8"]);
        self.set_accels_for_action("win.select-tab(8)", &["<alt>9"]);
    }
}
