#![feature(once_cell)]
use {gtk::prelude::*, mtnx::Application, std::error::Error};

fn main() -> Result<(), Box<dyn Error>> {
    #[cfg(debug_assertions)]
    std::env::set_var("RUST_LOG", "trace");

    logger::init();

    let app = Application::new();
    app.run();

    Ok(())
}
