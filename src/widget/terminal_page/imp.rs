use {
    crate::config::{Config, Preference},
    gtk::{
        gdk::RGBA,
        gio::Cancellable,
        glib::{
            self, translate::ToGlibPtr, ParamFlags, ParamSpec, ParamSpecString,
            SpawnFlags,
        },
        pango::{FontDescription, SCALE},
        prelude::*,
        subclass::prelude::*,
        CompositeTemplate, ScrolledWindow,
    },
    std::{
        cell::{OnceCell, RefCell},
        ops::Deref,
        sync::LazyLock,
    },
    vte::{PtyFlags, Terminal, TerminalExt, TerminalExtManual},
};

#[derive(Debug, CompositeTemplate)]
#[template(resource = "/com/gitlab/miridyan/Mt/Nx/ui/mt-terminal-page.ui")]
pub struct TerminalPage {
    #[template_child(id = "scrolled-window")]
    scrolled_window: TemplateChild<ScrolledWindow>,

    #[template_child(id = "terminal")]
    pub(super) terminal: TemplateChild<Terminal>,

    preferred_profile: RefCell<Option<Preference<String>>>,
    preferred_colorscheme: RefCell<Option<Preference<String>>>,
    // properties managed by gtk
    profile_name: OnceCell<String>,
    pub(super) title: RefCell<String>,
}

impl Default for TerminalPage {
    fn default() -> Self {
        log::trace!("+TerminalPage::default()");

        let page = Self {
            scrolled_window: TemplateChild::default(),
            terminal: TemplateChild::default(),
            preferred_profile: RefCell::default(),
            preferred_colorscheme: RefCell::default(),
            profile_name: OnceCell::default(),
            title: RefCell::default(),
        };

        log::trace!("-TerminalPage::default() -> {page:?}");
        page
    }
}

#[glib::object_subclass]
impl ObjectSubclass for TerminalPage {
    const NAME: &'static str = "MtTerminalPage";

    type Type = super::TerminalPage;
    type ParentType = gtk::Box;

    fn class_init(klass: &mut Self::Class) {
        log::trace!("+TerminalPage::class_init(klass: {klass:?})");
        Self::bind_template(klass);
        log::trace!("-TerminalPage::class_init()");
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        log::trace!("+TerminalPage::instance_init(obj: __unknown__)");
        obj.init_template();
        log::trace!("-TerminalPage::instance_init()");
    }
}

impl ObjectImpl for TerminalPage {
    fn constructed(&self, obj: &Self::Type) {
        log::trace!("+TerminalPage::constructed(obj: {obj:?})");
        self.parent_constructed(obj);
        // I'd prefer to do this within the template definition itself, but I don't know how to
        // do that lol
        let terminal = self.terminal.get();
        let scrolled_window = self.scrolled_window.get();
        let vadjustment = terminal.vadjustment();

        scrolled_window.set_vadjustment(vadjustment.as_ref());

        terminal
            .bind_property("window-title", obj, "window-title")
            .flags(
                glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE,
            )
            .build();

        log::trace!("-TerminalPage::constructed()");
    }

    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: LazyLock<Vec<ParamSpec>> = LazyLock::new(|| {
            vec![
                ParamSpecString::builder("profile-name")
                    .flags(ParamFlags::CONSTRUCT_ONLY | ParamFlags::READWRITE)
                    .build(),
                ParamSpecString::builder("window-title")
                    .flags(ParamFlags::READWRITE)
                    .build(),
            ]
        });

        PROPERTIES.as_ref()
    }

    fn property(
        &self,
        _obj: &Self::Type,
        _id: usize,
        pspec: &ParamSpec,
    ) -> glib::Value {
        match pspec.name() {
            "profile-name" => self.profile_name.get().to_value(),
            "window-title" => self.title.borrow().to_value(),
            _ => unimplemented!(),
        }
    }

    fn set_property(
        &self,
        _obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &ParamSpec,
    ) {
        match pspec.name() {
            "profile-name" => {
                let name = value
                    .get()
                    .expect("Failed to convert \"profile-name\" to string");
                // gtk should only allow us to set this once, so we shouldn't execute
                // twice. we're going to ignore this error.
                let _ = self.profile_name.set(name);
            }
            "window-title" => {
                let title = value
                    .get()
                    .expect("Failed to convert \"window-title\" to string");
                *self.title.borrow_mut() = title;
            }
            _ => unimplemented!(),
        }
    }
}

impl WidgetImpl for TerminalPage {}
impl BoxImpl for TerminalPage {}

impl TerminalPage {
    /// Initializes `self.terminal` with data stored in `self.profile` and `self.colorscheme`;
    pub fn init(&self, config: &Config) {
        log::trace!("+TerminalPage::init()");

        self.reload(config);

        let preferred_profile = self.preferred_profile.borrow();
        let preferred_colorscheme = self.preferred_colorscheme.borrow();

        assert!(
            preferred_profile.is_some(),
            "Preferred profile not set in init()"
        );
        assert!(
            preferred_colorscheme.is_some(),
            "Preferred colorscheme not set in init()"
        );

        let terminal = self.terminal.get();
        let profile_name = preferred_profile.as_ref().unwrap().actual();
        let profile = config
            .profile(&profile_name)
            .expect("Preferred profile must exist in config!");

        terminal
            .set_size(profile.dimensions.0 as i64, profile.dimensions.1 as i64);

        // I don't like this. I'm allocating twice for no reason.
        // let cmd_pathbufs = profile.cmd();
        let cmd = profile.cmd().map(std::path::Path::new).collect::<Vec<_>>();

        // Stop type checker from complaining
        let stop_complaining: Option<&Cancellable> = None;
        terminal.spawn_async(
            PtyFlags::DEFAULT,
            Some(&profile.cwd),
            // why on earth is argv a slice of `&Path`s? That makes no sense.
            cmd.as_slice(),
            &[],
            SpawnFlags::DEFAULT,
            // this is because `vte::Terminal` will refuse to create a new pty if an assert
            // fails, this empty function ensures that assert succeeds.
            Some(Box::new(|| {})),
            1000,
            stop_complaining,
            None,
        );

        log::trace!("-TerminalPage::init()");
    }

    /// Reads the stored values within `self.profile` and `self.colorscheme` and applies them
    /// to the `self.terminal`. Panics if it cannot borrow the current profile and colorscheme.
    pub fn reload(&self, config: &Config) {
        log::trace!("+TerminalPage::reload()");

        let terminal = self.terminal.get();

        let profile_name = self.profile_name.get().expect("Profile not set");
        let mut preferred_profile = Preference::from(profile_name.to_string());
        let profile = config.preferred_profile(&mut preferred_profile);

        let mut preferred_colorscheme =
            Preference::from(profile.colorscheme.to_string());
        let colorscheme =
            config.preferred_colorscheme(&mut preferred_colorscheme);

        *self.preferred_profile.borrow_mut() = Some(preferred_profile);
        *self.preferred_colorscheme.borrow_mut() = Some(preferred_colorscheme);

        let (bg, fg, _, colorscheme) = colorscheme.into_gdk();
        let colorscheme = colorscheme.iter().collect::<Vec<&RGBA>>();

        terminal.set_colors(Some(&fg), Some(&bg), colorscheme.as_slice());

        let mut font_description = FontDescription::new();

        font_description.set_family(&profile.font.family);
        font_description.set_weight(profile.font.weight.into());
        font_description.set_size(profile.font.size * SCALE);

        terminal.set_font_desc(Some(&font_description));
        terminal.set_scrollback_lines(profile.scrollback);
        terminal.set_audible_bell(profile.audible_bell);
        // terminal.set_size(
        //     profile.dimensions.0 as i64,
        //     profile.dimensions.1 as i64,
        // );
        terminal.set_cursor_blink_mode(profile.cursor_blink_mode.into());
        terminal.set_cursor_shape(profile.cursor_style.into());

        // Gross, why is this function not wrapped to be accessible in `vte`? I feel like
        // that would be really easy.
        unsafe {
            let terminal_ptr = terminal.to_glib_none().0;
            vte_sys::vte_terminal_set_allow_bold(
                terminal_ptr,
                profile.font.allow_bold as glib_sys::gboolean,
            );
        }

        log::trace!("-TerminalPage::reload()");
    }

    pub(super) fn colorscheme_preference(&self) -> Option<Preference<String>> {
        self.preferred_colorscheme
            .try_borrow()
            .ok()?
            .deref()
            .clone()
    }
}
