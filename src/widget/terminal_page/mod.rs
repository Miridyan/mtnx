mod imp;

use {
    crate::config::{Config, Preference},
    gtk::{gio, glib, glib::ObjectExt, subclass::prelude::*},
};

glib::wrapper! {
    pub struct TerminalPage(ObjectSubclass<imp::TerminalPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl TerminalPage {
    pub fn new(profile_name: &str, config: &Config) -> Self {
        log::trace!("+TerminalPage::new({profile_name:?}, {config:?})");
        let terminal =
            glib::Object::new::<Self>(&[("profile-name", &profile_name)])
                .init(config);

        log::trace!("-TerminalPage::new() -> {terminal:?}");
        terminal
    }

    /// Initializes `imp::TerminalPage.terminal` with data stored in `imp::TerminalPage.profile`
    /// and `imp::TerminalPage.colorscheme`;
    fn init(self, config: &Config) -> Self {
        log::trace!("+TerminalPage::init()");
        self.imp().init(config);
        log::trace!("-TerminalPage::init()");

        self
    }

    /// Reads the stored values within `imp::TerminalPage::profile` and
    /// `imp::TerminalPage.colorscheme` and applies them to the `imp::TerminalPage.terminal`.
    /// Panics if it cannot borrow the current profile and colorscheme.
    pub fn reload(&self, config: &Config) {
        log::trace!("+TerminalPage::reload()");
        self.imp().reload(config);
        log::trace!("+TerminalPage::reload()");
    }

    pub fn connect_title_changed<F: Fn(&Self) + 'static>(&self, f: F) {
        self.connect_notify_local(Some("window-title"), move |term, _| f(term));
    }

    pub fn colorscheme_preference(&self) -> Option<Preference<String>> {
        self.imp().colorscheme_preference()
    }
}

impl Default for TerminalPage {
    fn default() -> Self {
        log::trace!("+TerminalPage::default()");
        let terminal_page =
            glib::Object::new(&[]);

        log::trace!("-TerminalPage::default() -> {terminal_page:?}");
        terminal_page
    }
}
