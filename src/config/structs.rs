use {
    super::{enums::*, util},
    gtk::gdk::RGBA,
    serde::{Deserialize, Serialize},
};

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Globals {
    pub default_profile: String,
    pub fallback_colorscheme: String,
    pub scrollbar_visibility: ScrollbarVisibility,
    pub theme_variant: Theme,
    pub session_behavior: SessionBehavior,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Profile {
    pub interior_padding: u8,
    pub scrollback: i64,
    pub dimensions: (u32, u32),
    pub cell_spacing: (f32, f32),
    pub cursor_style: CursorShape,
    pub cursor_blink_mode: CursorBlinkMode,
    pub erase_binding: EraseBinding,
    pub audible_bell: bool,
    pub colorscheme: String,
    pub font: Font,
    pub cmd: String,
    pub cwd: String,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ColorScheme {
    pub bg: u32,
    pub fg: u32,
    pub window: u32,
    pub cursor: u32,
    pub colors: Vec<u32>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Font {
    pub allow_bold: bool,
    pub size: i32,
    pub family: String,
    pub weight: FontWeight,
}

impl Profile {
    #[doc(hidden)]
    pub fn default_scrollback() -> i64 {
        1_000
    }

    #[doc(hidden)]
    pub fn default_cmd() -> String {
        std::env::var("SHELL").unwrap()
    }

    #[doc(hidden)]
    pub fn default_cwd() -> String {
        std::env::var("HOME").unwrap()
    }

    #[doc(hidden)]
    pub fn default_colorscheme() -> String {
        String::from("Base16 Dark Default")
    }

    #[doc(hidden)]
    pub fn default_bell() -> bool {
        true
    }

    #[doc(hidden)]
    pub fn default_dimensions() -> (u32, u32) {
        (80, 25)
    }

    #[doc(hidden)]
    pub fn default_cell_spacing() -> (f32, f32) {
        (1.0, 1.0)
    }

    #[doc(hidden)]
    pub fn default_interior_padding() -> u8 {
        0
    }

    #[inline]
    pub fn cmd(&self) -> impl Iterator<Item = &str> {
        self.cmd.split(" ")
    }
}

impl ColorScheme {
    pub fn into_gdk(&self) -> (RGBA, RGBA, RGBA, Vec<RGBA>) {
        log::trace!("+ColorScheme::into_gdk()");
        let bg = util::u32_to_rgba(self.bg);
        let fg = util::u32_to_rgba(self.fg);
        let cursor = util::u32_to_rgba(self.cursor);
        let colorscheme = self
            .colors
            .iter()
            .map(|&c| util::u32_to_rgba(c))
            .collect::<Vec<RGBA>>();

        log::trace!("-ColorScheme::into_gdk() -> ({bg:?}, {fg:?}, {cursor:?}, {colorscheme:?})");
        (bg, fg, cursor, colorscheme)
    }
}

impl Font {
    #[doc(hidden)]
    pub fn default_size() -> i32 {
        10
    }

    #[doc(hidden)]
    pub fn default_family() -> String {
        "Monospace".to_string()
    }

    #[doc(hidden)]
    pub fn default_allow_bold() -> bool {
        true
    }
}

impl Default for Profile {
    fn default() -> Self {
        Self {
            interior_padding: Self::default_interior_padding(),
            scrollback: Self::default_scrollback(),
            dimensions: Self::default_dimensions(),
            cell_spacing: Self::default_cell_spacing(),
            cursor_style: Default::default(),
            cursor_blink_mode: Default::default(),
            erase_binding: Default::default(),
            audible_bell: Self::default_bell(),
            colorscheme: Self::default_colorscheme(),
            font: Default::default(),
            cwd: Self::default_cwd(),
            cmd: Self::default_cmd(),
        }
    }
}

#[rustfmt::skip]
impl Default for ColorScheme {
    fn default() -> Self {
        log::trace!("+ColorScheme::default()");

        let rc = Self {
            bg: 0x1d1f21ff,
            fg: 0xc5c8c6ff,
            window: 0x111213ff,
            cursor: 0xc5c8c6ff,
            colors: vec![
                //black     red         green       yellow      blue        purlple     aqua        white
                0x282a2eff, 0xa54242ff, 0x8c9440ff, 0xde935fff, 0x5f819dff, 0x85678fff, 0x5e8d87ff, 0x707880ff, 
                0x373b41ff, 0xcc6666ff, 0xb5bd68ff, 0xf0c674ff, 0x81a2beff, 0xb294bbff, 0x8abeb7ff, 0xc5c8c6ff,
            ],
        };

        log::trace!("-ColorScheme::default() -> {rc:?}");
        rc
    }
}

impl Default for Font {
    fn default() -> Self {
        Self {
            allow_bold: Font::default_allow_bold(),
            size: Font::default_size(),
            family: Font::default_family(),
            weight: FontWeight::default(),
        }
    }
}
