use {
    gtk::gdk::RGBA,
    regex::{Captures, Regex},
    std::{
        env, fs,
        ops::Deref,
        path::{Path, PathBuf},
        str,
    },
};

const R_MASK: u32 = 0xff000000;
const G_MASK: u32 = 0x00ff0000;
const B_MASK: u32 = 0x0000ff00;
const A_MASK: u32 = 0x000000ff;
pub const ENV_REGEX: &'static str = r"\$[A-Za-z0-9_]+";

/// Attempt to identify, resolve, and replace all environment variables. After a run through
/// the string, check to see if any environment variables were not replaced. If all were
/// replaced, execute `f(parsed_str)`, if one or more was not, execute `g(parsed_str)`.
pub fn parse_env_regex_on_fail<T, F, G>(
    mut path: String,
    f: F,
    g: G,
) -> Option<T>
where
    F: Fn(String) -> Option<T>,
    G: Fn(String) -> Option<T>,
{
    let reg = Regex::new(ENV_REGEX).expect("Failed to parse ENV_REGEX");

    if reg.is_match(&path) {
        path = reg
            .replace_all(&path, |caps: &Captures<'_>| {
                match env::var(&caps[0][1..]) {
                    Ok(val) => val,
                    Err(e) => {
                        let unchanged = caps[0].to_string();
                        log::warn!(
                            "Could not find environment variable {} : {}",
                            unchanged,
                            e
                        );
                        unchanged
                    }
                }
            })
            .deref()
            .to_string();
    }

    if !reg.is_match(&path) {
        f(path)
    } else {
        g(path)
    }
}

/// Like `parse_env_regex_on_fail`, but in the event that any environment variable cannot be
/// resolved, return `None`.
pub fn parse_env_regex<T, F>(path: String, f: F) -> Option<T>
where
    F: Fn(String) -> Option<T>,
{
    parse_env_regex_on_fail(path, f, |_| None)
}

/// Given a string that represents a path that may potentially contain environment variables,
/// parse out and replace all environment variables. Once finished, attempt to canonicalize
/// the path with `std::fs::canonicalize`. If either the replacement of environment variables
/// or path canonicalization fails, return `None`.
pub fn parse_path(path: String) -> Option<PathBuf> {
    parse_env_regex(path, |r_path| {
        fs::canonicalize(&Path::new(&r_path))
            .or_else(|err| {
                log::warn!("Failed to canonicalize path {:?}", err);
                Err(err)
            })
            .ok()
    })
}

/// Break a command into two parts, the command and the parameters. For all parts of the command,
/// parse out and replace any and all environment variables. If that variable cannot be resolved,
/// just don't replace it and pass the string on. For the command, check if the command exists
/// in the filesystem. If not, search the path for the command and if found prepend that
/// respective portion of the $PATH to the command.
pub fn parse_cmd(cmd: String) -> Option<String> {
    let mut split = cmd
        .splitn(2, " ")
        .map(String::from)
        .collect::<Vec<String>>();
    let cmd = parse_env_regex(split[0].clone(), |p_cmd| {
        fs::metadata(&p_cmd)
            .ok()
            // TODO:
            // use `bool::then()` here when it's stabilized, can make this cleaner and
            // remove all if statements from closures
            .and_then(|metadata| Some(metadata.is_file()))
            .or_else(|| Some(false))
            .and_then(|is_file| {
                if !is_file {
                    env::var_os("PATH").and_then(|paths| {
                        env::split_paths(&paths)
                            .filter_map(|dir| {
                                let full = dir.join(p_cmd.deref().to_owned());
                                // will be able to use `bool::then()` here too
                                // no more ifs!
                                if full.is_file() {
                                    log::info!(
                                        "Found command file at {:?}",
                                        full
                                    );
                                    full.to_str().map(String::from)
                                } else {
                                    None
                                }
                            })
                            .next()
                    })
                } else {
                    Some(p_cmd)
                }
            })
    });

    if let Some(new_cmd) = cmd {
        split[0] = new_cmd;
    } else {
        return None;
    }

    if split.len() == 2 {
        // If the environment variable fails to resolve, just return the string anyway
        // I'll let the software you're calling deal with it. Who knows, maybe it can export
        // some value to environment variable? Anyway, it's not my problem, so I'll just let
        // it fall through.
        let f = |p_param| Some(p_param);
        let params = parse_env_regex_on_fail(split[1].clone(), f, f);

        if let Some(new_params) = params {
            log::info!("Command parameters {}", new_params);
            split[1] = new_params;
        } else {
            return None;
        }
    }

    Some(split.join(" "))
}

/// I can't implement `Into<T>` or `From<T>` for `u32` or `gdk::RGBA` so I guess this is
/// my only choice
pub fn u32_to_rgba(c: u32) -> RGBA {
    let r = (c & R_MASK) >> 24;
    let g = (c & G_MASK) >> 16;
    let b = (c & B_MASK) >> 8;
    let a = c & A_MASK;

    RGBA::builder()
        .red(r as f32 / 255.0)
        .green(g as f32 / 255.0)
        .blue(b as f32 / 255.0)
        .alpha(a as f32 / 255.0)
        .build()
}
