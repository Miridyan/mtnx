use {
    crate::new_config::{
        self, util, ColorScheme, CursorBlinkMode, CursorShape, EraseBinding,
        FontWeight, ScrollbarVisibility, SessionBehavior, Theme,
    },
    serde::{Deserialize, Deserializer, Serialize},
    std::collections::HashMap,
};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct FileBackend {
    #[serde(default = "FileBackend::default_profile")]
    pub default_profile: String,

    #[serde(default)]
    pub scrollbar: ScrollbarVisibility,

    #[serde(default)]
    session_behavior: SessionBehavior,

    #[serde(default)]
    theme: Theme,

    #[serde(default)]
    interior_padding: u8,

    #[serde(default)]
    pub colorschemes: HashMap<String, ColorScheme>,

    #[serde(default)]
    pub profiles: Store,
}

#[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct Store {
    defaults: Option<Profile>,
    profiles: HashMap<String, Profile>,
}

/// Structure describing the basic terminal profile (what shell to use, what colorscheme to use,
/// what font to use, what size to make the terminal when it launches for the first time, open new
/// terminals in a new window or new tab, etc, etc, etc).
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Profile {
    pub interior_padding: Option<u8>,
    pub scrollback: Option<i64>,
    pub dimensions: Option<(u32, u32)>,
    pub cell_spacing: Option<(f32, f32)>,
    pub cursor_style: Option<CursorShape>,
    pub cursor_blink_mode: Option<CursorBlinkMode>,
    pub erase_binding: Option<EraseBinding>,
    pub audible_bell: Option<bool>,
    pub colorscheme: Option<String>,
    pub font: Option<Font>,

    #[serde(deserialize_with = "Profile::deserialize_cmd")]
    pub cmd: Option<String>,
    #[serde(deserialize_with = "Profile::deserialize_cwd")]
    pub cwd: Option<String>,
}

/// Structure describes either a custom font or whether to use the default system font. This
/// structure can be converted into a `pango::FontDescription`
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Font {
    pub allow_bold: Option<bool>,
    pub size: Option<i32>,
    pub family: Option<String>,
    pub weight: Option<FontWeight>,
}

impl FileBackend {
    fn default_profile() -> String {
        "Default".to_string()
    }
}

impl Store {
    pub(super) fn remove(&mut self, profile: &str) -> Option<Profile> {
        self.profiles.remove(profile)
    }

    pub(super) fn take_default(&mut self) -> Option<Profile> {
        self.defaults.take()
    }

    pub(super) fn drain(
        &mut self,
    ) -> std::collections::hash_map::Drain<String, Profile> {
        self.profiles.drain()
    }
}

impl From<Font> for new_config::Font {
    fn from(f: Font) -> Self {
        Self {
            allow_bold: f.allow_bold.unwrap_or(Self::default_allow_bold()),
            family: f.family.unwrap_or(Self::default_family()),
            size: f.size.unwrap_or(Self::default_size()),
            weight: f.weight.unwrap_or_default(),
        }
    }
}

impl From<Profile> for new_config::Profile {
    fn from(p: Profile) -> Self {
        Self {
            interior_padding: p
                .interior_padding
                .unwrap_or(Self::default_interior_padding()),
            scrollback: p.scrollback.unwrap_or(Self::default_scrollback()),
            dimensions: p.dimensions.unwrap_or(Self::default_dimensions()),
            cell_spacing: p
                .cell_spacing
                .unwrap_or(Self::default_cell_spacing()),
            cursor_style: p.cursor_style.unwrap_or_default(),
            cursor_blink_mode: p.cursor_blink_mode.unwrap_or_default(),
            erase_binding: p.erase_binding.unwrap_or_default(),
            audible_bell: p.audible_bell.unwrap_or(Self::default_bell()),
            font: p.font.map(new_config::Font::from).unwrap_or_default(),
            colorscheme: p.colorscheme.unwrap_or(Self::default_colorscheme()),
            cmd: p.cmd.unwrap_or(Self::default_cmd()),
            cwd: p.cwd.unwrap_or(Self::default_cwd()),
        }
    }
}

impl super::Fallback for Font {
    type Source = Font;

    fn fallback(&mut self, other: &Self::Source) {
        self.allow_bold.fallback(&other.allow_bold);
        self.size.fallback(&other.size);
        self.weight.fallback(&other.weight);

        self.family = self
            .family
            .as_ref()
            .or(other.family.as_ref())
            .map(String::from)
    }
}

impl super::Fallback for Profile {
    type Source = Self;

    fn fallback(&mut self, other: &Self::Source) {
        self.interior_padding.fallback(&other.interior_padding);
        self.scrollback.fallback(&other.scrollback);
        self.dimensions.fallback(&other.dimensions);
        self.cell_spacing.fallback(&other.cell_spacing);
        self.cursor_style.fallback(&other.cursor_style);
        self.cursor_blink_mode.fallback(&other.cursor_blink_mode);
        self.erase_binding.fallback(&other.erase_binding);
        self.audible_bell.fallback(&other.audible_bell);

        self.font = {
            let font = self.font.take();
            font.zip(other.font.as_ref()).map(|(mut s, o)| {
                s.fallback(o);
                s
            })
        };

        self.colorscheme = self
            .colorscheme
            .as_ref()
            .or(other.colorscheme.as_ref())
            .map(String::from);

        self.cmd = self.cmd.as_ref().or(other.cmd.as_ref()).map(String::from);

        self.cwd = self.cwd.as_ref().or(other.cwd.as_ref()).map(String::from);
    }
}

impl Profile {
    /// When deserializing the string for the command, parse any environment variables
    /// and check the first word in the string to see if it a) is a valid commmand path,
    /// or b) if that item exists in the path somewhere.
    fn deserialize_cmd<'de, D>(
        deserializer: D,
    ) -> Result<Option<String>, D::Error>
    where
        D: Deserializer<'de>,
    {
        Option::deserialize(deserializer)
            .map(|opt| opt.and_then(util::parse_cmd))
    }

    /// When deserializing the string for the working directory, parse any environment
    /// variables and verify that it is a valid path.
    fn deserialize_cwd<'de, D>(
        deserializer: D,
    ) -> Result<Option<String>, D::Error>
    where
        D: Deserializer<'de>,
    {
        Option::deserialize(deserializer)
            .map(|opt| opt.and_then(util::parse_path))
            .map(|opt| opt.and_then(|p| p.to_str().map(String::from)))
    }
}
