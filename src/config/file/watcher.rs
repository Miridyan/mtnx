use {
    super::{load_from_path, File},
    crate::event::AppEvent,
    gtk::glib,
    notify::{
        recommended_watcher, RecommendedWatcher, RecursiveMode,
        Watcher as NotifyWatcher,
    },
    std::{
        cell::OnceCell,
        fmt,
        path::{Path, PathBuf},
    },
    xdg::BaseDirectories,
};

pub struct Watcher {
    xdg: BaseDirectories,
    path: PathBuf,
    base_path: PathBuf,
    watcher: OnceCell<RecommendedWatcher>,
    sender: glib::Sender<AppEvent>,
}

impl Watcher {
    pub fn new(sender: glib::Sender<AppEvent>) -> crate::Result<Self> {
        log::trace!("+Watcher::new(sender: {sender:?})");

        let xdg = BaseDirectories::with_prefix(super::MT_XDG_PREFIX)?;
        let base_path = xdg.get_config_home();
        log::trace!("XDG_CONFIG_HOME={base_path:?}");

        let create_result = xdg.create_config_directory(&base_path);
        log::trace!("Create XDG config dir result {create_result:?}");

        let path = {
            let mut path = base_path.clone();
            path.push("config.ron");

            path
        };

        let _ = create_result?;
        let ret = Ok(Self {
            xdg,
            base_path,
            path,
            watcher: OnceCell::new(),
            sender,
        });

        log::trace!("-Watcher::new() -> {ret:?}");
        ret
    }

    // slight problem with this. vim deletes files when it modifies them. this can
    // be a bit problematic because it seems to stop the watcher. so if you change
    // the config once in vim, it will never be reloaded. so I'm probably going
    // to change it to recursively watch the config dir and look for changes
    // to files that match the name of the config
    pub fn start(&self) -> crate::Result<()> {
        let sender = self.sender.clone();
        let mut watcher = recommended_watcher(
            move |res: Result<notify::event::Event, _>| {
                watcher_fn(res, &sender);
            },
        )?;
        watcher.watch(&self.base_path, RecursiveMode::Recursive)?;

        if let Err(_watcher) = self.watcher.set(watcher) {
            log::error!("Failed to set watcher in OnceCell");
        }

        if let Err(err) = self.sender.send(AppEvent::ConfigFileWatcherStarted) {
            log::error!(
                "Failed to send watcher start notification to mainloop: {err}"
            );
        }

        Ok(())
    }

    pub fn read(&self) -> crate::Result<File> {
        load_from_path(&self.path)
    }

    pub fn path(&self) -> &Path {
        &self.path
    }
}

fn watcher_fn(
    res: Result<notify::event::Event, impl std::error::Error>,
    sender: &glib::Sender<AppEvent>,
) {
    log::trace!("+Watcher::watcher");

    use notify::event::{
        AccessKind::*, AccessMode::*, EventKind::*, ModifyKind::*,
        RenameMode::*,
    };

    match res {
        Ok(event) => {
            log::trace!("Watcher(Event({:?}))", event.kind);

            if event.kind == Access(Close(Write)) ||
                event.kind == Modify(Name(From))
            {
                log::info!("{event:?}");

                if let Err(err) =
                    sender.send(AppEvent::ConfigFileChanged(event.paths))
                {
                    log::error!("Failed to send ConfigFileChanged event to mainloop: {err}");
                }
            }
        }
        Err(e) => log::error!("{e:?}"),
    }

    log::trace!("-Watcher::watcher");
}

// INotifyWatcher doesn't implement debug, probably a way around this that is
// more elegant, but this works for now.
impl fmt::Debug for Watcher {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Point")
            .field("xdg", &self.xdg)
            .field("path", &self.path)
            .field("watcher", &"__unknown__")
            .finish()
    }
}
