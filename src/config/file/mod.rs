mod watcher;

pub use watcher::Watcher;

static MT_XDG_PREFIX: &'static str = "Mt";

use {
    crate::config::{
        self, util, ColorScheme, CursorBlinkMode, CursorShape, EraseBinding,
        FontWeight, ScrollbarVisibility, SessionBehavior, Theme,
    },
    serde::{Deserialize, Deserializer, Serialize},
    std::{
        collections::{hash_map, HashMap},
        fs,
        path::Path,
    },
};

trait Fallback {
    type Source;

    fn fallback(&mut self, other: &Self::Source);
}

impl<T> Fallback for Option<T>
where
    T: Copy,
{
    type Source = Option<T>;

    fn fallback(&mut self, other: &Self::Source) {
        *self = self.or(*other);
    }
}

pub fn load_from_path(path: impl AsRef<Path>) -> crate::Result<File> {
    let file = fs::File::open(&path)?;
    let options = ron::options::Options::default()
        .with_default_extension(ron::extensions::Extensions::IMPLICIT_SOME);

    options.from_reader(file).map_err(|err| {
        log::error!("Unable to load profile : {:?}, {:?}", path.as_ref(), err);
        err.into()
    })
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct File {
    #[serde(default = "File::default_profile")]
    default_profile: String,
    #[serde(default = "File::fallback_colorscheme")]
    fallback_colorscheme: String,
    #[serde(default)]
    scrollbar: ScrollbarVisibility,
    #[serde(default)]
    session_behavior: SessionBehavior,
    #[serde(default)]
    theme: Theme,
    #[serde(default)]
    interior_padding: u8,
    #[serde(default)]
    colorschemes: HashMap<String, ColorScheme>,
    #[serde(default)]
    profiles: Store,
}

#[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
struct Store {
    #[serde(default)]
    defaults: Option<Profile>,
    #[serde(default)]
    profiles: HashMap<String, Profile>,
}

/// Structure describing the basic terminal profile (what shell to use, what colorscheme to use,
/// what font to use, what size to make the terminal when it launches for the first time, open new
/// terminals in a new window or new tab, etc, etc, etc).
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
struct Profile {
    #[serde(default)]
    pub interior_padding: Option<u8>,
    #[serde(default)]
    pub scrollback: Option<i64>,
    #[serde(default)]
    pub dimensions: Option<(u32, u32)>,
    #[serde(default)]
    pub cell_spacing: Option<(f32, f32)>,
    #[serde(default)]
    pub cursor_style: Option<CursorShape>,
    #[serde(default)]
    pub cursor_blink_mode: Option<CursorBlinkMode>,
    #[serde(default)]
    pub erase_binding: Option<EraseBinding>,
    #[serde(default)]
    pub audible_bell: Option<bool>,
    #[serde(default)]
    pub colorscheme: Option<String>,
    #[serde(default)]
    pub font: Option<Font>,

    #[serde(default, deserialize_with = "Profile::deserialize_cmd")]
    pub cmd: Option<String>,
    #[serde(default, deserialize_with = "Profile::deserialize_cwd")]
    pub cwd: Option<String>,
}

/// Structure describes either a custom font or whether to use the default system font. This
/// structure can be converted into a `pango::FontDescription`
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
struct Font {
    #[serde(default)]
    pub allow_bold: Option<bool>,
    #[serde(default)]
    pub size: Option<i32>,
    #[serde(default)]
    pub family: Option<String>,
    #[serde(default)]
    pub weight: Option<FontWeight>,
}

pub struct ProfileIterator<'a> {
    c: hash_map::Drain<'a, String, Profile>,
    d: Option<Profile>,
    s: Option<&'a str>,
}

pub struct ColorschemeIterator<'a> {
    c: hash_map::Drain<'a, String, ColorScheme>,
}

impl File {
    fn default_profile() -> String {
        "Default".to_string()
    }

    fn fallback_colorscheme() -> String {
        "Fallback".to_string()
    }

    pub fn profiles(&mut self) -> ProfileIterator {
        let d = self.profiles.take_default();
        let c = self.profiles.drain();
        let s = &self.default_profile;

        ProfileIterator { c, d, s: Some(s) }
    }

    pub fn colorschemes(&mut self) -> ColorschemeIterator {
        if !self.colorschemes.contains_key(&self.fallback_colorscheme) {
            self.colorschemes.insert(
                self.fallback_colorscheme.to_owned(),
                ColorScheme::default(),
            );
        }

        assert!(self.colorschemes.contains_key(&self.fallback_colorscheme));

        ColorschemeIterator {
            c: self.colorschemes.drain(),
        }
    }
}

impl Store {
    #[inline]
    fn take_default(&mut self) -> Option<Profile> {
        self.defaults.take()
    }

    #[inline]
    fn drain(&mut self) -> hash_map::Drain<String, Profile> {
        self.profiles.drain()
    }
}

impl From<Font> for config::Font {
    fn from(f: Font) -> Self {
        Self {
            allow_bold: f.allow_bold.unwrap_or(Self::default_allow_bold()),
            family: f.family.unwrap_or(Self::default_family()),
            size: f.size.unwrap_or(Self::default_size()),
            weight: f.weight.unwrap_or_default(),
        }
    }
}

impl From<Profile> for config::Profile {
    fn from(p: Profile) -> Self {
        Self {
            interior_padding: p
                .interior_padding
                .unwrap_or(Self::default_interior_padding()),
            scrollback: p.scrollback.unwrap_or(Self::default_scrollback()),
            dimensions: p.dimensions.unwrap_or(Self::default_dimensions()),
            cell_spacing: p
                .cell_spacing
                .unwrap_or(Self::default_cell_spacing()),
            cursor_style: p.cursor_style.unwrap_or_default(),
            cursor_blink_mode: p.cursor_blink_mode.unwrap_or_default(),
            erase_binding: p.erase_binding.unwrap_or_default(),
            audible_bell: p.audible_bell.unwrap_or(Self::default_bell()),
            font: p.font.map(config::Font::from).unwrap_or_default(),
            colorscheme: p.colorscheme.unwrap_or(Self::default_colorscheme()),
            cmd: p.cmd.unwrap_or(Self::default_cmd()),
            cwd: p.cwd.unwrap_or(Self::default_cwd()),
        }
    }
}

// impl From<&File> for config::Globals {
//     fn from(f: &File) -> Self {
//         config::Globals {
//             default_profile: f.default_profile.to_owned(),
//             fallback_colorscheme: f.fallback_colorscheme.to_owned(),
//             scrollbar_visibility: f.scrollbar,
//             theme_variant: f.theme,
//             session_behavior: f.session_behavior,
//         }
//     }
// }

impl From<File> for config::Globals {
    fn from(f: File) -> Self {
        config::Globals {
            default_profile: f.default_profile,
            fallback_colorscheme: f.fallback_colorscheme,
            scrollbar_visibility: f.scrollbar,
            theme_variant: f.theme,
            session_behavior: f.session_behavior,
        }
    }
}

impl Fallback for Font {
    type Source = Font;

    fn fallback(&mut self, other: &Self::Source) {
        self.allow_bold.fallback(&other.allow_bold);
        self.size.fallback(&other.size);
        self.weight.fallback(&other.weight);

        self.family = self
            .family
            .as_ref()
            .or(other.family.as_ref())
            .map(String::from)
    }
}

impl Fallback for Profile {
    type Source = Self;

    fn fallback(&mut self, other: &Self::Source) {
        self.interior_padding.fallback(&other.interior_padding);
        self.scrollback.fallback(&other.scrollback);
        self.dimensions.fallback(&other.dimensions);
        self.cell_spacing.fallback(&other.cell_spacing);
        self.cursor_style.fallback(&other.cursor_style);
        self.cursor_blink_mode.fallback(&other.cursor_blink_mode);
        self.erase_binding.fallback(&other.erase_binding);
        self.audible_bell.fallback(&other.audible_bell);

        self.font = {
            let font = self.font.take();
            font.zip(other.font.as_ref())
                .map(|(mut s, o)| {
                    s.fallback(o);
                    s
                })
                .or(other.font.clone())
        };

        self.colorscheme = self
            .colorscheme
            .as_ref()
            .or(other.colorscheme.as_ref())
            .map(String::from);

        self.cmd = self.cmd.as_ref().or(other.cmd.as_ref()).map(String::from);
        self.cwd = self.cwd.as_ref().or(other.cwd.as_ref()).map(String::from);
    }
}

impl Profile {
    /// When deserializing the string for the command, parse any environment variables
    /// and check the first word in the string to see if it a) is a valid commmand path,
    /// or b) if that item exists in the path somewhere.
    fn deserialize_cmd<'de, D>(
        deserializer: D,
    ) -> Result<Option<String>, D::Error>
    where
        D: Deserializer<'de>,
    {
        Option::deserialize(deserializer)
            .map(|opt| opt.and_then(util::parse_cmd))
    }

    /// When deserializing the string for the working directory, parse any environment
    /// variables and verify that it is a valid path.
    fn deserialize_cwd<'de, D>(
        deserializer: D,
    ) -> Result<Option<String>, D::Error>
    where
        D: Deserializer<'de>,
    {
        Option::deserialize(deserializer)
            .map(|opt| opt.and_then(util::parse_path))
            .map(|opt| opt.and_then(|p| p.to_str().map(String::from)))
    }
}

impl<'a> Iterator for ProfileIterator<'a> {
    type Item = (String, config::Profile);

    fn next(&mut self) -> Option<Self::Item> {
        match self.c.next() {
            Some((name, mut profile)) => {
                if let Some(ref default_profile) = self.d {
                    profile.fallback(default_profile);
                }

                if self.s == Some(&name) {
                    self.s.take();
                }

                Some((name, config::Profile::from(profile)))
            }
            None => {
                let name = self.s.take()?;
                let profile = self.d.take()?;

                Some((String::from(name), config::Profile::from(profile)))
            }
        }
    }
}

impl<'a> Iterator for ColorschemeIterator<'a> {
    type Item = (String, ColorScheme);

    fn next(&mut self) -> Option<Self::Item> {
        self.c.next()
    }
}
