mod enums;
pub mod file;
mod structs;
pub mod util;

pub use enums::*;
pub use file::{load_from_path, File};
pub use structs::*;

use {
    gtk::gio::{self, ResourceLookupFlags},
    std::collections::HashMap,
};

#[derive(Clone, Debug)]
pub struct Preference<T> {
    preferred: T,
    actual: Option<T>,
}

impl<T> Preference<T> {
    pub fn is_preferred(&self) -> bool {
        !self.actual.is_some()
    }

    pub fn actual(&self) -> &T {
        self.actual.as_ref().unwrap_or(&self.preferred)
    }
}

impl<T: std::fmt::Display> std::fmt::Display for Preference<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.actual())
    }
}

impl<T: ToOwned> From<T> for Preference<<T as ToOwned>::Owned> {
    fn from(t: T) -> Self {
        Self {
            preferred: t.to_owned(),
            actual: None,
        }
    }
}

#[derive(Debug)]
pub struct Config {
    pub globals: Globals,
    profiles: HashMap<String, Profile>,
    colorschemes: HashMap<String, ColorScheme>,
}

impl Config {
    pub fn preferred_profile(
        &self,
        preference: &mut Preference<String>,
    ) -> &Profile {
        match self.profile(&preference.preferred) {
            Some(profile) => {
                preference.actual = None;
                profile
            }
            None => {
                let profile = self.profile(&self.globals.default_profile);
                assert!(
                    profile.is_some(),
                    "Default profile not present in config"
                );

                preference.actual =
                    Some(self.globals.default_profile.to_owned());
                profile.unwrap()
            }
        }
    }

    pub fn profile(&self, name: &str) -> Option<&Profile> {
        self.profiles.get(name)
    }

    pub fn preferred_colorscheme(
        &self,
        preference: &mut Preference<String>,
    ) -> &ColorScheme {
        match self.colorscheme(&preference.preferred) {
            Some(colorscheme) => {
                preference.actual = None;
                colorscheme
            }
            None => {
                let colorscheme =
                    self.colorscheme(&self.globals.fallback_colorscheme);
                assert!(
                    colorscheme.is_some(),
                    "Fallback colorscheme not present in config"
                );

                preference.actual =
                    Some(self.globals.fallback_colorscheme.to_owned());
                colorscheme.unwrap()
            }
        }
    }

    pub fn colorscheme(&self, name: &str) -> Option<&ColorScheme> {
        self.colorschemes.get(name)
    }

    pub fn styles(&self) -> HashMap<String, String> {
        self.colorschemes
            .keys()
            .map(|colorscheme| {
                (
                    String::from(colorscheme),
                    colorscheme.replace(" ", "-").to_lowercase(),
                )
            })
            .collect()
    }

    pub fn stylesheet(
        &self,
        styles: &HashMap<String, String>,
    ) -> Option<String> {
        let resource = gio::resources_lookup_data(
            "/com/gitlab/miridyan/Mt/Nx/colorscheme.css",
            ResourceLookupFlags::NONE,
        )
        .ok()?;

        let stylesheet_base = std::str::from_utf8(&resource).ok()?.to_string();

        let stylesheets = self
            .colorschemes
            .iter()
            .filter_map(|(key, value)| {
                let style_class = styles.get(key)?;
                let window = format!("{:08x}", value.bg);
                let tab = format!("{:08x}", value.window);

                Some(
                    stylesheet_base
                        .replace("{{colorscheme}}", &style_class)
                        .replace("{{tab-active}}", &tab)
                        .replace("{{titlebar}}", &window),
                )
            })
            .fold(String::new(), |mut stylesheet, profile_stylesheet| {
                stylesheet.push_str(&profile_stylesheet);
                stylesheet.push_str("\n");
                stylesheet
            });

        Some(stylesheets)
    }
}

impl Default for Config {
    fn default() -> Self {
        let globals = Globals::default();
        let mut profiles = HashMap::<String, Profile>::default();
        let mut colorschemes = HashMap::<String, ColorScheme>::default();

        let profile = Profile::default();
        let colorscheme = ColorScheme::default();

        // TODO: make sure nothing bad happens from fallback colorscheme
        colorschemes.insert(String::from(&profile.colorscheme), colorscheme);
        profiles.insert(String::from(&globals.default_profile), profile);

        Config {
            globals,
            profiles,
            colorschemes,
        }
    }
}

impl From<File> for Config {
    fn from(mut f: File) -> Self {
        let colorschemes =
            f.colorschemes().collect::<HashMap<String, ColorScheme>>();
        let mut profiles = f.profiles().collect::<HashMap<String, Profile>>();
        let globals = Globals::from(f);

        assert!(profiles.contains_key(&globals.default_profile));
        assert!(colorschemes.contains_key(&globals.fallback_colorscheme));

        profiles.iter_mut().for_each(|(_, mut profile)| {
            if !colorschemes.contains_key(&profile.colorscheme) {
                profile.colorscheme = globals.fallback_colorscheme.clone();
            }
        });

        Self {
            globals,
            profiles,
            colorschemes,
        }
    }
}
