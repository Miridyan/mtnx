use {
    crate::config::Preference,
    adw::{subclass::prelude::*, HeaderBar, TabView},
    gtk::{
        glib,
        glib::{Binding, FromVariant},
        prelude::*,
        CompositeTemplate,
    },
    std::{cell::RefCell, collections::HashMap},
};

#[derive(Debug, CompositeTemplate)]
#[template(resource = "/com/gitlab/miridyan/Mt/Nx/ui/mt-window.ui")]
pub struct ApplicationWindow {
    #[template_child(id = "titlebar")]
    pub titlebar: TemplateChild<HeaderBar>,

    #[template_child(id = "terminal-tab-view")]
    pub tabview: TemplateChild<TabView>,

    title_binding: RefCell<Option<Binding>>,

    colorscheme_style: RefCell<HashMap<String, String>>,
    active_window_style: RefCell<Option<String>>,
}

impl Default for ApplicationWindow {
    fn default() -> Self {
        log::trace!("+ApplicationWindow::default()");
        let window = Self {
            titlebar: TemplateChild::default(),
            tabview: TemplateChild::default(),
            title_binding: RefCell::default(),
            colorscheme_style: RefCell::default(),
            active_window_style: RefCell::default(),
        };

        log::trace!("-ApplicationWindow::default() -> {window:?}");
        window
    }
}

#[glib::object_subclass]
impl ObjectSubclass for ApplicationWindow {
    const NAME: &'static str = "MtApplicationWindow";

    type Type = super::ApplicationWindow;
    type ParentType = adw::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        log::trace!("+ApplicationWindow::class_init(klass: {klass:?})");
        Self::bind_template(klass);

        klass.install_action("win.new-tab", None, move |window, _, _| {
            window.create_tab(None);
        });

        klass.install_action(
            "win.select-tab",
            Some("i"),
            move |window, _, page| {
                if let Some(page) = page.and_then(i32::from_variant) {
                    window.select_tab(page);
                }
            },
        );

        klass.install_action(
            "win.close-tab",
            Some("i"),
            move |window, _, page| {
                if let Some(page) = page.and_then(i32::from_variant) {
                    window.close_tab(Some(page));
                }
            },
        );

        klass.install_action(
            "win.close-current-tab",
            None,
            move |window, _, _| {
                window.close_tab(None);
            },
        );

        log::trace!("-ApplicationWindow::class_init()");
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        log::trace!("+ApplicationWindow::instance_init(obj: __unknown__)");
        obj.init_template();
        log::trace!("-ApplicationWindow::instance_init()");
    }
}

impl ObjectImpl for ApplicationWindow {
    fn constructed(&self, obj: &Self::Type) {
        log::trace!("+ApplicationWindow::constructed(obj: {obj:?})");
        self.parent_constructed(obj);

        if crate::app::APPID.ends_with("Devel") {
            obj.add_css_class("devel");
        }

        obj.connect_tabview();

        log::trace!("-ApplicationWindow::constructed()");
    }
}

impl WidgetImpl for ApplicationWindow {}
impl WindowImpl for ApplicationWindow {}
impl ApplicationWindowImpl for ApplicationWindow {}
impl AdwApplicationWindowImpl for ApplicationWindow {}

impl ApplicationWindow {
    pub(super) fn replace_binding(
        &self,
        new_binding: Binding,
    ) -> Option<Binding> {
        self.title_binding.replace(Some(new_binding))
    }

    pub(super) fn set_style_map(&self, styles: HashMap<String, String>) {
        if let Ok(mut colorscheme_style) =
            self.colorscheme_style.try_borrow_mut()
        {
            *colorscheme_style = styles;
        }
    }

    pub(super) fn set_style(
        &self,
        obj: &<Self as ObjectSubclass>::Type,
        colorscheme: &Preference<String>,
    ) {
        let mut current_window_style =
            self.active_window_style.try_borrow_mut().ok();
        let colorscheme_style = self.colorscheme_style.try_borrow().ok();
        let new_style = colorscheme_style
            .as_ref()
            .and_then(|cs| cs.get(colorscheme.actual()));

        if let Some(style) = new_style {
            obj.add_css_class(style);

            if let Some(ref mut current) = current_window_style {
                if let Some(ref old) = current.replace(style.to_string()) {
                    if style != old {
                        obj.remove_css_class(old);
                    }
                }
            }
        }
    }
}
