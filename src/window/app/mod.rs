mod imp;

use {
    crate::{
        app::Application,
        config::{Config, Preference},
        widget::TerminalPage,
    },
    adw::TabView,
    gtk::{
        gio,
        glib::{self, clone},
        prelude::*,
        subclass::prelude::*,
    },
    std::{collections::HashMap, ops::Deref},
};

glib::wrapper! {
    pub struct ApplicationWindow(ObjectSubclass<imp::ApplicationWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl ApplicationWindow {
    pub fn new(app: &Application) -> Self {
        log::trace!("+ApplicationWindow::new(app: __unknown__)");
        let window: Self = glib::Object::new::<Self>(&[
            ("application", &app),
            ("icon-name", &crate::app::APPID),
            ("title", &"Mt"),
        ])
        .styles(app.styles());

        log::trace!("-ApplicationWindow::new() -> {window:?}");
        window
    }

    /// Create function for creating a new window when a tab is dropped from
    /// a tabview
    fn tabview_create(app: &Application) -> Option<TabView> {
        let window = Self::new(app);

        window.present();
        Some(window.tabview().clone())
    }

    pub fn create_tab(&self, profile_name: Option<&str>) {
        log::trace!(
            "+ApplicationWindow::tab_page(profile_name: {profile_name:?})"
        );

        let gtk_app = self
            .application()
            .expect("No application for current window!");

        let app = gtk_app
            .downcast_ref::<Application>()
            .expect("Cannot cast Application to mtnx::app::Application");

        if let Ok(config) = app.config() {
            let config_ref = config.deref();
            let profile_name =
                profile_name.unwrap_or(&config_ref.globals.default_profile);

            let terminal_page = TerminalPage::new(profile_name, config_ref);
            self.append_page(&terminal_page);
        }

        log::trace!("-ApplicationWindow::tab_page()");
    }

    fn styles(self, styles: Option<HashMap<String, String>>) -> Self {
        if let Some(styles) = styles {
            self.imp().set_style_map(styles);
        }
        self
    }

    pub fn close_tab(&self, page: Option<i32>) {
        let tabview = self.tabview();
        let num_pages = tabview.n_pages();

        match page {
            Some(page) => {
                if 0 <= page && page < num_pages {
                    tabview.close_page(&tabview.nth_page(page));
                }
            }
            None => {
                if let Some(ref page) = tabview.selected_page() {
                    tabview.close_page(page);
                }
            }
        }

        // if the last page has been closed, close the window. we need to get
        // the value again because the previous operation can fail or succeed
        // depending on the validitiy of input
        if tabview.n_pages() == 0 {
            self.close();
        }
    }

    pub fn select_tab(&self, tab_number: i32) {
        let tabview = self.tabview();
        let num_pages = tabview.n_pages();

        if 0 <= tab_number && tab_number < num_pages {
            tabview.set_selected_page(&tabview.nth_page(tab_number));
        }
    }

    pub fn tabview(&self) -> &TabView {
        &self.imp().tabview
    }

    pub fn append_page(&self, terminal_page: &TerminalPage) {
        log::trace!(
            "+ApplicationWindow::append_page(terminal_page: {terminal_page:?})"
        );
        let tabview = self.tabview();
        let tabpage = tabview.append(terminal_page);

        terminal_page
            .bind_property("window-title", &tabpage, "title")
            .flags(
                glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE,
            )
            .build();

        // trying this, because it seems when the tooltip is unconfigured, it falls
        // back to using the tab page title. this would be fine, but ive noticed it causes
        // the terminal to lock up sometimes, not quite sure why that is, but it usually
        // happens when showing the tooltip. maybe setting it separately will make things
        // function a bit more smoothly
        terminal_page
            .bind_property("window-title", &tabpage, "tooltip")
            .flags(
                glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE,
            )
            .build();

        log::trace!("-ApplicationWindow::append_page()");
    }

    pub fn reload_config(&self, config: &Config) {
        let styles = config.styles();
        self.imp().set_style_map(styles);
        self.for_each_tab(|terminal| {
            terminal.reload(config);
        });

        // need to refresh the active window style, this may become its own function
        // someday.
        if let Some(child) =
            self.tabview().selected_page().map(|page| page.child())
        {
            if let Some(terminal_page) = child.downcast_ref::<TerminalPage>() {
                if let Some(ref preference) =
                    terminal_page.colorscheme_preference()
                {
                    self.set_style(preference);
                }
            }
        }
    }

    fn for_each_tab<F>(&self, f: F)
    where
        F: Fn(&TerminalPage),
    {
        let tabview = self.tabview();
        for i in 0..tabview.n_pages() {
            let page = tabview.nth_page(i);
            if let Some(t) = page.child().downcast_ref::<TerminalPage>() {
                f(t)
            }
        }
    }

    fn connect_tabview(&self) {
        self.tabview().connect_selected_page_notify(
            clone!(@weak self as window => move |_| {
                window.selected_page_changed();
            }),
        );

        // clone!() macro doesn't allow a lambda that doesn't return `()`
        // at least as far as I can tell.
        let window = self.clone();
        self.tabview().connect_create_window(move |_| {
            let gtk_app = window.application()?;

            gtk_app
                .downcast_ref::<Application>()
                .and_then(|app| super::ApplicationWindow::tabview_create(app))
        });
    }

    fn selected_page_changed(&self) {
        if let Some(ref page) = self.tabview().selected_page() {
            if let Some(terminal) = page.child().downcast_ref::<TerminalPage>()
            {
                self.bind_title(terminal, "window-title");
                if let Some(ref preference) = terminal.colorscheme_preference()
                {
                    self.set_style(preference);
                }
            }
        }
    }

    fn bind_title<O>(&self, source: &O, property_name: &str)
    where
        O: IsA<glib::Object>,
    {
        let new_binding = source
            .bind_property(property_name, self, "title")
            .flags(
                glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE,
            )
            .build();

        // I'm pretty worried about a memory leak here. I _think_ it's fine
        // but I don't trust gtk to deallocate the binding once I'm done here
        if let Some(old_binding) = self.imp().replace_binding(new_binding) {
            old_binding.unbind();
        }
    }

    fn set_style(&self, colorscheme: &Preference<String>) {
        self.imp().set_style(self, colorscheme);
    }
}
