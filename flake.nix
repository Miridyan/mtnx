{
  description = "Mt, a terminal emulator for GTK4 and LibAdwaita";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
      in
      with pkgs;
      {
        devShells.default = mkShell {
          buildInputs = [
            gcc
            gtk4
            libadwaita
            pkg-config
            rust-analyzer
            vte-gtk4
            (rust-bin.selectLatestNightlyWith(toolchain: toolchain.default))
          ];
        };
      }
    );
}
